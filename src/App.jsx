import React, { useEffect, useState } from 'react';
import Header from './components/Header/Header';
import Courses from './components/Courses/Courses';
import { Routes, Route, BrowserRouter, Navigate } from 'react-router-dom';
import { v4 } from 'uuid';

import { mockedCoursesList } from './data/mockedCourses';
import { mockedAuthorsList } from './data/mockedAuthors';

import './App.css';
import CreateCourse from './components/CreateCourse/CreateCourse';
import { getCurrentDateFormatted } from './helpers/datePipe';
import { TOKEN_LOCAL_STORAGE } from './constants';
import { Registration } from './components/Registration/Registration';
import Login from './components/Login/Login';
import { loadUser } from './services/UserService';
import CourseInfo from './components/CourseInfo/CourseInfo';

function App() {
	const [courses, setCourses] = useState(mockedCoursesList);
	const [authors, setAuthors] = useState(mockedAuthorsList);
	const [userLoggedIn, setUserLoggedIn] = useState(null);
	const [currentUser, setCurrentUser] = useState(null);

	useEffect(() => {
		checkUserLoggedIn();
	}, []);

	useEffect(() => {
		loadCurrentUser();
	}, [userLoggedIn]);

	const loadCurrentUser = async () => {
		if (userLoggedIn) {
			try {
				const res = await loadUser(localStorage.getItem(TOKEN_LOCAL_STORAGE));
				const obj = await res.json();
				if (res.status !== 200) {
					alert('Loading current user failed');
				} else {
					setCurrentUser(obj.result);
				}
			} catch (err) {
				alert(err);
			}
		} else {
			setCurrentUser(null);
		}
	};

	const checkUserLoggedIn = () => {
		console.log(
			'User logged in: ',
			!!localStorage.getItem(TOKEN_LOCAL_STORAGE)
		);
		setUserLoggedIn(!!localStorage.getItem(TOKEN_LOCAL_STORAGE));
	};

	const addNewAuthor = (name) => {
		setAuthors(
			authors.concat({
				name,
				id: v4(),
			})
		);
	};

	const addNewCourse = (title, description, authors, duration) => {
		setCourses(
			courses.concat({
				title,
				id: v4(),
				description,
				authors,
				duration,
				creationDate: getCurrentDateFormatted(),
			})
		);
	};

	const logout = () => {
		localStorage.removeItem(TOKEN_LOCAL_STORAGE);
		setUserLoggedIn(false);
		setCurrentUser(null);
	};

	if (userLoggedIn === null) {
		return null;
	}

	return (
		<div>
			<BrowserRouter>
				<Header
					logout={logout}
					user={currentUser}
					userLoggedIn={userLoggedIn}
				/>
				<Routes>
					{!userLoggedIn && (
						<>
							<Route
								path='/login'
								exact
								element={<Login checkUserLoggedIn={checkUserLoggedIn} />}
							/>
							<Route path='/registration' exact element={<Registration />} />
							<Route path='*' element={<Navigate to='/login' />} />
						</>
					)}
					{userLoggedIn && (
						<>
							<Route
								exact
								path='/courses'
								element={<Courses courses={courses} authors={authors} />}
							/>
							<Route
								path='/courses/:courseId'
								element={<CourseInfo courses={courses} authors={authors} />}
							/>
							<Route
								exact
								path='courses/add'
								element={
									<CreateCourse
										addNewCourse={addNewCourse}
										addNewAuthor={addNewAuthor}
										authors={authors}
									/>
								}
							/>
							<Route path='*' element={<Navigate to='/courses' />} />
						</>
					)}
				</Routes>
			</BrowserRouter>
		</div>
	);
}

export default App;
