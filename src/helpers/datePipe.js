import { valuesPipe } from './valuesPipe';

export const dateConverterPipe = (dateStr) => {
	const [day, month, year] = dateStr.split('/');
	const dayStr = valuesPipe(+day);
	const monthStr = valuesPipe(+month);
	const yearStr = valuesPipe(+year);
	return `${dayStr}.${monthStr}.${yearStr}`;
};

export const getCurrentDateFormatted = () => {
	const date = new Date().toISOString();
	const [year, month, day] = date.slice(0, 10).split('-');
	return `${day}/${month}/${year}`;
};
