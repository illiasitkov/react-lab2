import { valuesPipe } from './valuesPipe';

export const durationPipe = (mins) => {
	mins = mins < 0 ? 0 : mins;
	const hours = Math.trunc(mins / 60);
	const minutes = mins - hours * 60;
	return { hours, minutes };
};

export const durationPipeToString = (mins) => {
	const { hours, minutes } = durationPipe(mins);
	let hoursStr = valuesPipe(+hours);
	let minutesStr = valuesPipe(+minutes);
	return `${hoursStr}:${minutesStr}`;
};
