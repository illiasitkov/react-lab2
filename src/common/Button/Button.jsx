import React from 'react';
import './Button.css';
import PropTypes from 'prop-types';

const Button = ({ buttonText, onClick, type }) => (
	<button type={type} onClick={onClick}>
		{buttonText}
	</button>
);

Button.propTypes = {
	buttonText: PropTypes.string.isRequired,
	onClick: PropTypes.func,
	type: PropTypes.oneOf(['button', 'submit']).isRequired,
};

Button.defaultProps = {
	type: 'button',
};

export default Button;
