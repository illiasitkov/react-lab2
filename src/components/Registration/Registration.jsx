import React, { useState } from 'react';

import './Registration.css';
import Input from '../../common/Input/Input';
import { changeHandler } from '../../helpers/changeHandler';
import Button from '../../common/Button/Button';
import { Link, useNavigate } from 'react-router-dom';
import { registerNewUser } from '../../services/AuthService';

export const Registration = () => {
	const [name, setName] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	const navigate = useNavigate();

	const registerUser = async (e) => {
		e.preventDefault();
		try {
			const res = await registerNewUser(name, email, password);
			if (res.status !== 201) {
				alert('Registration failed: ' + res.statusText);
			} else {
				navigate('/login', { replace: true });
			}
		} catch (err) {
			alert(err);
		}
	};

	return (
		<div className='row content-wrapper'>
			<div className='mt-5 offset-md-4 offset-2 col-md-4 col-8'>
				<h3 className='text-center mb-2'>Registration</h3>
				<form onSubmit={registerUser}>
					<Input
						onChange={changeHandler(setName)}
						value={name}
						width='100%'
						id='name'
						inputPlaceholder='Enter name'
						labelText='Name'
						type='text'
					/>
					<Input
						onChange={changeHandler(setEmail)}
						value={email}
						width='100%'
						id='email'
						inputPlaceholder='Enter email'
						labelText='Email'
						type='email'
					/>
					<Input
						onChange={changeHandler(setPassword)}
						value={password}
						width='100%'
						id='password'
						inputPlaceholder='Enter password'
						labelText='Password'
						type='password'
					/>
					<div className='d-flex flex-column align-items-center gap-2'>
						<Button type='submit' buttonText='Registration' />
						<p>
							If you have an account you can <Link to='/login'>Login</Link>
						</p>
					</div>
				</form>
			</div>
		</div>
	);
};
