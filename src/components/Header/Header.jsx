import React from 'react';
import './Header.css';
import { Logo } from './components/Logo/Logo';
import { LOGOUT } from '../../constants';
import Button from '../../common/Button/Button';
import PropTypes from 'prop-types';

const Header = ({ user, userLoggedIn, logout }) => {
	return (
		<header className='d-flex justify-content-between align-items-center flex-wrap'>
			<Logo />
			<div className='d-flex gap-4'>
				{user && <div className='username'>{user.name}</div>}
				{userLoggedIn && <Button buttonText={LOGOUT} onClick={logout} />}
			</div>
		</header>
	);
};

Header.propTypes = {
	user: PropTypes.shape({
		name: PropTypes.string.isRequired,
	}),
	userLoggedIn: PropTypes.bool.isRequired,
	logout: PropTypes.func.isRequired,
};

export default Header;
