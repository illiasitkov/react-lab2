import React from 'react';
import './CourseCard.css';
import Button from '../../../../common/Button/Button';
import CourseDetails from './components/CourseDetails/CourseDetails';
import { SHOW_COURSE } from '../../../../constants';
import { useNavigate } from 'react-router-dom';
import PropTypes from 'prop-types';

const CourseCard = ({ course, authors }) => {
	const navigate = useNavigate();

	const navigateToCourse = () => {
		navigate(`/courses/${course.id}`, { replace: true });
	};

	return (
		<div className='course-card'>
			<div className='row'>
				<div className='col-8'>
					<h2 className='mb-3'>
						<strong>{course.title}</strong>
					</h2>
					<div>{course.description}</div>
				</div>
				<div className='col-4'>
					<CourseDetails course={course} authors={authors} showId={false} />
					<div className='mt-4 d-flex justify-content-center'>
						<Button buttonText={SHOW_COURSE} onClick={navigateToCourse} />
					</div>
				</div>
			</div>
		</div>
	);
};

CourseCard.propTypes = {
	course: PropTypes.shape({
		id: PropTypes.string.isRequired,
		title: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
	}).isRequired,
	authors: PropTypes.array.isRequired,
};

export default CourseCard;
