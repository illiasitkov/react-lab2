import React, { useState } from 'react';
import CourseCard from './components/CourseCard/CourseCard';
import SearchBar from './components/SearchBar/SearchBar';
import Button from '../../common/Button/Button';
import { useNavigate } from 'react-router-dom';
import { BTN_ADD_COURSE } from '../../constants';
import PropTypes from 'prop-types';

const Courses = ({ courses, authors }) => {
	const navigate = useNavigate();

	const [searchStr, setSearchStr] = useState('');

	const filter = (string) => (course) => {
		let stringLower = string.toLowerCase();
		return (
			course.title.toLowerCase().includes(stringLower) ||
			course.id.toLowerCase().includes(stringLower)
		);
	};

	const navigateToCreateCourse = () => {
		navigate('/courses/add', { replace: true });
	};

	const views = courses.filter(filter(searchStr)).map((course) => {
		return (
			<div key={course.id} className='mb-4'>
				<CourseCard course={course} authors={authors} />
			</div>
		);
	});

	return (
		<section className='content-wrapper'>
			<div className='d-flex justify-content-between align-items-center mb-4 flex-wrap'>
				<SearchBar onSearch={setSearchStr} />
				<Button buttonText={BTN_ADD_COURSE} onClick={navigateToCreateCourse} />
			</div>
			<div>{views}</div>
		</section>
	);
};

Courses.propTypes = {
	courses: PropTypes.arrayOf(
		PropTypes.shape({
			id: PropTypes.string.isRequired,
		})
	).isRequired,
	authors: PropTypes.array.isRequired,
};

export default Courses;
