import React, { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import CourseDetails from '../Courses/components/CourseCard/components/CourseDetails/CourseDetails';

import './CourseInfo.css';
import PropTypes from 'prop-types';

const CourseInfo = ({ courses, authors }) => {
	const [course, setCourse] = useState(null);

	const params = useParams();

	useEffect(() => {
		setCourse(courses.filter((c) => c.id === params.courseId)[0]);
	}, []);

	return (
		<div className='content-wrapper'>
			<Link to='/courses'>{'< Back to courses'}</Link>
			{course ? (
				<>
					<h1 className='text-center'>{course.title}</h1>
					<div className='row'>
						<p className='col-6'>{course.description}</p>
						<div className='offset-1 col-5'>
							<CourseDetails showId={true} course={course} authors={authors} />
						</div>
					</div>
				</>
			) : (
				<h2>Loading...</h2>
			)}
		</div>
	);
};

CourseInfo.propTypes = {
	courses: PropTypes.arrayOf(
		PropTypes.shape({
			id: PropTypes.string.isRequired,
			title: PropTypes.string.isRequired,
			description: PropTypes.string.isRequired,
		})
	).isRequired,
	authors: PropTypes.array.isRequired,
};

export default CourseInfo;
